﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float force;
	public float x;
	public GameObject gameController;

	private bool isBirdAlive;

	private AudioSource audioSource;
	public AudioClip flapAC;
	public AudioClip hitAC;
	public AudioClip fallAC;

	// Use this for initialization
	void Start() {

		GetComponent<Rigidbody2D>().isKinematic = true;

		// Look for audiosource in main camera
		audioSource = Camera.main.GetComponent <AudioSource>();
		if (audioSource == null)
			{
				Debug.LogError("Error finding Audio Source in main camera");
			}	
	}
	
	// Update is called once per frame
	void Update() {
	
		Debug.Log("alive " + isBirdAlive);
		Debug.Log("kinematic " + GetComponent<Rigidbody2D>().isKinematic);



		if (Input.GetKeyDown(KeyCode.Space) && isBirdAlive)
			{
				if (this.transform.position.y < Camera.main.orthographicSize)
					{
						this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * force);
					}
			}
	}

	void OnTriggerExit2D(Collider2D other) {
		Debug.Log("trigger" + isBirdAlive);
		
		if (isBirdAlive)
			{
				Debug.Log("alive");
				gameController.gameObject.GetComponent<GameController>().incrementScore();
				audioSource.PlayOneShot(flapAC);
			}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		Debug.Log("Game Over");
		
		if (coll.gameObject.tag == "Pipe")
			{
				GetComponent<Animator>().SetInteger("state", 2);
				this.GetComponent<Rigidbody2D>().AddForce(new Vector2(force / 2, force));
				isBirdAlive = false;
				audioSource.PlayOneShot(hitAC);

			} else if (isBirdAlive)
			{
				GetComponent<Animator>().SetInteger("state", 5);
				audioSource.PlayOneShot(fallAC);
				isBirdAlive = false;
			} else
			{
				GetComponent<Animator>().SetInteger("state", 3);
				audioSource.PlayOneShot(fallAC);

			}
			
		gameController.gameObject.GetComponent<GameController>().loseGame();

	}

	public void startToFly() {
		isBirdAlive = true;
		GetComponent<Rigidbody2D>().isKinematic = false;
		GetComponent<Animator>().SetInteger("state", 1);
	}
}
 