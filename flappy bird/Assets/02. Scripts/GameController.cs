﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour {

	public Text scoreUI;
	public GameObject player;
	public GameObject tutorial;
	private int score;

	private bool inGame;
	private bool restartable;

	private AudioSource audioSource;
	public AudioClip scoreAC;
	public AudioClip gameOverAC;
	public AudioClip mainAC;


	// Use this for initialization
	void Start() {
		score = 0;
		inGame = false;
		restartable = false;
		
		// Look for audiosource in main camera
		audioSource = Camera.main.GetComponent <AudioSource>();
		if (audioSource == null)
			{
				Debug.LogError("Error finding Audio Source in main camera");
			} else
			{
				playMainTheme();
			}
	}
	
	// Update is called once per frame
	void Update() {
	
		tutorial.gameObject.SetActive(!inGame);

		if (Input.GetKeyDown(KeyCode.Space) && !inGame)
			{
				audioSource.loop = false;
				audioSource.Stop();
				inGame = true;
				GetComponent<PipeScript>().startPipeGeneration();
				player.gameObject.GetComponent<PlayerController>().startToFly();
			}
		
		if (Input.GetKeyDown(KeyCode.Space) && restartable)
			{
				SceneManager.LoadScene("Game_Scene");
			}

		scoreUI.text = "Score: " + score;
	}

	public void incrementScore() {
		score++;
		audioSource.PlayOneShot(scoreAC);
	}

	public void loseGame() {
		inGame = false;
		restartable = true;
		GetComponent<PipeScript>().stopPipeGeneration();
		audioSource.PlayOneShot(gameOverAC);
	}

	private void playMainTheme() {
		audioSource.loop = true;
		audioSource.PlayOneShot(mainAC);
	}
}
