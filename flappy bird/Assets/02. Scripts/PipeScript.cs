﻿using UnityEngine;
using System.Collections;

public class PipeScript : MonoBehaviour {

	public GameObject pipePrefab;

	public float gameSpeed;
	public float distanceBetweenPipes;
	public float y;

	private float distanceAcum;
	private bool generate;

	// Use this for initialization
	void Start() {
		distanceAcum = 3f;
	}
	
	// Update is called once per frame
	void Update() {
	
		if (generate)
			{
				GameObject[] pipes = GameObject.FindGameObjectsWithTag("Pipe");

				Vector3 movement = new Vector3(-gameSpeed * Time.deltaTime, 0f, 0f);
				distanceAcum += gameSpeed * Time.deltaTime;

				foreach (GameObject pipe in pipes)
					{
						pipe.gameObject.transform.Translate(movement);

						if (pipe.gameObject.transform.position.x < -10f)
							{
								Destroy(pipe.gameObject);
							}
					}

				if (distanceAcum > distanceBetweenPipes)
					{	
						y = Random.Range(0.5f, 4.0f);

						Vector3 spawnPos = new Vector3(8f, y, 0.0f);
						Instantiate(pipePrefab, spawnPos, Quaternion.identity);
						distanceAcum = 0f;
					}			
			}
	}

	public void stopPipeGeneration() {
		generate = false;
	}

	public void startPipeGeneration() {
		generate = true;
	}
}
